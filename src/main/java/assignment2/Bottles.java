package assignment2;

class Bottles {

	public String verse(int verseNumber) {
		String s1 = Integer.toString(verseNumber);
		String s2 = Integer.toString(verseNumber-1);
		if(verseNumber == 2){
			return	s1 + " bottles of beer on the wall, " + s1 +" bottles of beer.\n"
					+ "Take one down and pass it around, " + s2 +" bottle of beer on the wall.\n";

		}
		if(verseNumber == 1){
			return "1 bottle of beer on the wall, " + "1 bottle of beer.\n" + "Take it down and pass it around, "
					+ "no more bottles of beer on the wall.\n";
		}
		if(verseNumber == 0){
			return "No more bottles of beer on the wall, " + "no more bottles of beer.\n"
					+ "Go to the store and buy some more, " + "99 bottles of beer on the wall.\n";
		}
		return s1 + " bottles of beer on the wall, " + s1 +" bottles of beer.\n"
				+ "Take one down and pass it around, " + s2 +" bottles of beer on the wall.\n";
	}


	public String verse(int startVerseNumber, int endVerseNumber) {
		String str= "";
		for (int i = startVerseNumber; i>=endVerseNumber; i--){
			if(i == endVerseNumber){
				str = str + verse(i);
			}
			else{
				str = str + verse(i) + "\n";
			}
		}
		return str;
	}




	public String song() {
		return verse(99,0);
	}

}
